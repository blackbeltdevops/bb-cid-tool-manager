package register

import (
    "net/http"
    "encoding/json"
    "bitbucket.org/blackbeltdevops/bb-cid-tool-manager/lib"
    "bitbucket.org/blackbeltdevops/bb-cid-tool-manager/bbGithubApi"
    "bitbucket.org/blackbeltdevops/bb-cid-tool-manager/bbBuildkiteApi"
    "bitbucket.org/blackbeltdevops/bb-cid-tool-manager/bbProjInit"
)

var rL = routeList{}

type routeList struct {
    Routes []string
}

func GetFuncRouter() []lib.FunctionRouter {
    return [] lib.FunctionRouter{
        lib.FunctionRouter{RouteName: "/", Func: Http200Ok}.Gen(),
    }
}

func Http200Ok(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Content-Type", "application/json")
    w.Header().Set("Server", "Cid tool manager webserver")
    json.NewEncoder(w).Encode(rL)
}

func Register() {
    for _, funcRouter := range GetFuncRouter() {
        http.HandleFunc(funcRouter.RouteName, funcRouter.Func)
        rL.Routes = append(rL.Routes, funcRouter.RouteName)
    }
    for _, funcRouter := range bbGithubApi.GetFuncRouter() {
        http.HandleFunc(funcRouter.RouteName, funcRouter.Func)
        rL.Routes = append(rL.Routes, funcRouter.RouteName)
    }
    for _, funcRouter := range bbBuildkiteApi.GetFuncRouter() {
        http.HandleFunc(funcRouter.RouteName, funcRouter.Func)
        rL.Routes = append(rL.Routes, funcRouter.RouteName)
    }
    for _, funcRouter := range bbProjInit.GetFuncRouter() {
        http.HandleFunc(funcRouter.RouteName, funcRouter.Func)
        rL.Routes = append(rL.Routes, funcRouter.RouteName)
    }
}
