package bbProjInit

import (
    "bytes"
    "fmt"
    "io"
    "net/http"
    "net/http/httptest"
    "net/url"
    "os"
    "testing"

    "bitbucket.org/blackbeltdevops/bb-buildkite-api/bb-buildkite"
    "bitbucket.org/blackbeltdevops/bb-cid-tool-manager/bbBuildkiteApi"
    "bitbucket.org/blackbeltdevops/bb-cid-tool-manager/bbGithubApi"
    "github.com/buildkite/go-buildkite/buildkite"
    "github.com/google/go-github/github"
    "github.com/stretchr/testify/assert"
    "strings"
)

const (
    // baseURLPath is a non-empty Client.BaseURL path to use during tests,
    // to ensure relative URLs are used for all endpoints. See issue #752.
    baseURLPath      = "/api-v3"
    pipelineResponse = `{
    "id": "4c1e3756-f13a-40ca-8729-8c71c0487b92",
    "url": "https://api.buildkite.com/v2/organizations/bbcommonorg/pipelines/my-pipeline",
    "web_url": "https://buildkite.com/bbcommonorg/my-pipeline",
    "name": "My Pipeline",
    "description": null,
    "slug": "my-pipeline",
    "repository": "git@github.com:acme-inc/my-pipeline.git",
    "branch_configuration": null,
    "default_branch": "master",
    "skip_queued_branch_builds": false,
    "skip_queued_branch_builds_filter": null,
    "cancel_running_branch_builds": false,
    "cancel_running_branch_builds_filter": null,
    "provider": {
        "id": "github",
        "settings": {
            "trigger_mode": "code",
            "build_pull_requests": true,
            "pull_request_branch_filter_enabled": false,
            "skip_pull_request_builds_for_existing_commits": true,
            "build_pull_request_forks": false,
            "prefix_pull_request_fork_branch_names": true,
            "build_tags": false,
            "publish_commit_status": true,
            "publish_commit_status_per_step": false,
            "repository": "acme-inc/my-pipeline"
        },
        "webhook_url": "https://webhook.buildkite.com/deliver/97ca3476df2f4746ee1868d342f8ef327b299c2c4e25017b3c"
    },
    "builds_url": "https://api.buildkite.com/v2/organizations/bbcommonorg/pipelines/my-pipeline/builds",
    "badge_url": "https://badge.buildkite.com/0ff4e26efc4577ecd6641472471c0493779d3dbcb4007b52c5.svg",
    "created_at": "2018-06-05T12:46:31.064Z",
    "env": {},
    "scheduled_builds_count": 0,
    "running_builds_count": 0,
    "scheduled_jobs_count": 0,
    "running_jobs_count": 0,
    "waiting_jobs_count": 0,
    "steps": [
        {
            "type": "script",
            "name": "Build :package:",
            "command": "script/release.sh",
            "artifact_paths": null,
            "branch_configuration": null,
            "env": {},
            "timeout_in_minutes": null,
            "agent_query_rules": [],
            "concurrency": null,
            "parallelism": null
        }
    ]
}`
)

func BKsetup() (client *buildkite.Client, mux *http.ServeMux, serverURL string, teardown func()) {
    mux = http.NewServeMux()
    server := httptest.NewServer(mux)
    client = buildkite.NewClient(http.DefaultClient)
    sUrl, _ := url.Parse(server.URL)
    client.BaseURL = sUrl
    return client, mux, server.URL, server.Close
}

func GHsetup() (client *github.Client, mux *http.ServeMux, serverURL string, teardown func()) {
    // mux is the HTTP request multiplexer used with the test server.
    mux = http.NewServeMux()

    // We want to ensure that tests catch mistakes where the endpoint URL is
    // specified as absolute rather than relative. It only makes a difference
    // when there's a non-empty base URL path. So, use that. See issue #752.
    apiHandler := http.NewServeMux()
    apiHandler.Handle(baseURLPath+"/", http.StripPrefix(baseURLPath, mux))
    apiHandler.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
        fmt.Fprintln(os.Stderr, "FAIL: Client.BaseURL path prefix is not preserved in the request URL:")
        fmt.Fprintln(os.Stderr)
        fmt.Fprintln(os.Stderr, "\t"+req.URL.String())
        fmt.Fprintln(os.Stderr)
        fmt.Fprintln(os.Stderr, "\tDid you accidentally use an absolute endpoint URL rather than relative?")
        fmt.Fprintln(os.Stderr, "\tSee https://bbGithubGlobal-github.com/google/go-bbGithubGlobal-github/issues/752 for information.")
        http.Error(w, "Client.BaseURL path prefix is not preserved in the request URL.", http.StatusInternalServerError)
    })

    // server is a test HTTP server used to provide mock API responses.
    server := httptest.NewServer(apiHandler)

    // client is the GitHub client being tested and is
    // configured to use test server.
    client = github.NewClient(nil)
    cliUrl, _ := url.Parse(server.URL + baseURLPath + "/")
    client.BaseURL = cliUrl
    client.UploadURL = cliUrl

    return client, mux, server.URL, server.Close
}

type nopCloser struct {
    io.Reader
}

func (nopCloser) Close() error { return nil }

var bEmpty = nopCloser{bytes.NewBufferString("")}

func req(method string, body nopCloser) *http.Request {
    return &http.Request{
        Method: method,
        URL:    &url.URL{Host: "/Invite/Push"},
        Body:   body,
    }
}

func Test_bbProjInitApi_CreatePipelineAndHook(t *testing.T) {
    BKcli, BKmux, _, BKteardown := BKsetup()
    GHcli, GHmux, _, GHteardown := GHsetup()
    defer BKteardown()
    defer GHteardown()

    testOrg := "BlackBeltTechnology"
    owner := "BlackBeltTechnology"
    repo := "ci-integrator"

    BKmux.HandleFunc("/v2/organizations/"+testOrg+"/pipelines",
        func(w http.ResponseWriter, r *http.Request) {
            fmt.Fprint(w, pipelineResponse)
        })
    GHmux.HandleFunc("/repos/"+owner+"/"+repo+"/hooks", func(w http.ResponseWriter, r *http.Request) {
        fmt.Fprint(w, `{"id":1}`)
    })

    fakeBK := func() bbBuildkiteApi.BBbuildKiteApi {
        return bbBuildkiteApi.BBbuildKiteApi{
            &bb_buildkite.BBBuildkite{Client: BKcli}, nil,
        }
    }

    fakeGH := func() bbGithubApi.BBGithubAPi {
        return bbGithubApi.BBGithubAPi{
            GhCli: GHcli,
        }
    }

    //fakeGH := bbGithubApi.BBGithubAPi{
    //    GhCli: GHcli,
    //}

    jsonAsStr := `{
      "buildKiteData": {
        "token": "___GITHUB_TOKEN____",
        "organization": "%v",
        "pipelineConfig": {
          "name": "TestAddPipeLineViaREST",
          "repository": "git@bitbucket.org:blackbeltdevops/nexus-reposize-plugin.git",
          "steps": [
            {
              "type": "script",
              "name": "Build :hammer:",
              "command": "buildkite-agent pipeline upload"
            }
          ]
        }
      },
      "githubData": {
        "token": "___BUILDKITE_TOKEN____",
        "owner": "%v",
        "repository": "%v",
        "hook": {
          "name": "web",
          "active": true,
          "events": [
            "push",
            "pull_request"
          ],
          "config": {
            "url": "http://example.com/webhook",
            "content_type": "json"
          }
        }
      }
    }`
    jsonAsStr = fmt.Sprintf(jsonAsStr, testOrg, testOrg, repo)

    successFakeW := httptest.NewRecorder()
    successFakeReq := req("POST", nopCloser{bytes.NewBufferString(jsonAsStr)})

    failJsonFakeW := httptest.NewRecorder()
    failJsonFakeReq := req("POST", nopCloser{bytes.NewBufferString(`{{{{{{{`)})

    type fields struct {
        bk bbBuildkiteApi.BBbuildKiteApi
        gh bbGithubApi.BBGithubAPi
    }
    type args struct {
        w http.ResponseWriter
        r *http.Request
    }
    tests := []struct {
        name   string
        fields fields
        args   args
    }{
        {"success", fields{fakeBK(), fakeGH()}, args{successFakeW, successFakeReq}},
        {"jsonFail", fields{fakeBK(), fakeGH()}, args{failJsonFakeW, failJsonFakeReq}},
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            bPI := &bbProjInitApi{
                buildKiteApi: tt.fields.bk,
                githubAPi:    tt.fields.gh,
            }
            bPI.CreatePipelineAndHook(tt.args.w, tt.args.r)
            fmt.Println(tt.args.w)
            if tt.name == "success" {
                assert.Equal(t, 200, successFakeW.Code)
            }
            if tt.name == "jsonFail" {
                assert.Equal(t, 400, failJsonFakeW.Code)
                assert.Equal(t, `{"jsonError":"invalid character '{' looking for beginning of object key string"}`, failJsonFakeW.Body.String())
            }
        })
    }
}

func Test_bbProjInitApi_makeContextAndClient(t *testing.T) {
    type fields struct {
        buildKiteApi         bbBuildkiteApi.BBbuildKiteApi
        githubAPi            bbGithubApi.BBGithubAPi
        invitedUserGithubApi bbGithubApi.BBGithubAPi
    }
    type args struct {
        tokenGH          string
        tokenBK          string
        invitedUserToken string
    }
    tests := []struct {
        name   string
        fields fields
        args   args
    }{
    // TODO: Add test cases.
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            bPI := &bbProjInitApi{
                buildKiteApi:         tt.fields.buildKiteApi,
                githubAPi:            tt.fields.githubAPi,
                invitedUserGithubApi: tt.fields.invitedUserGithubApi,
            }
            bPI.makeContextAndClient(tt.args.tokenGH, tt.args.tokenBK, tt.args.invitedUserToken)
        })
    }
}

func Test_bbProjInitApi_Init(t *testing.T) {
    BKcli, BKmux, _, BKteardown := BKsetup()
    GHcli, GHmux, _, GHteardown := GHsetup()
    defer GHteardown()
    defer BKteardown()

    org := "BlackBeltTechnology"
    owner := "BlackBeltTechnology"
    repo := "ci-integrator"
    user := "___INVITED_GITHUB_USER_NAME____"
    path := "__FILE_PATH__"

    BKmux.HandleFunc("/v2/organizations/"+org+"/pipelines",
        func(w http.ResponseWriter, r *http.Request) {
            fmt.Fprint(w, pipelineResponse)
        })

    GHmux.HandleFunc("/repos/"+owner+"/"+repo+"/collaborators/"+user, func(w http.ResponseWriter, r *http.Request) {
        w.WriteHeader(http.StatusNoContent)
    })
    GHmux.HandleFunc("/user/repository_invitations", func(w http.ResponseWriter, r *http.Request) {
        w.WriteHeader(http.StatusNoContent)
    })
    GHmux.HandleFunc("/user/repository_invitations/1", func(w http.ResponseWriter, r *http.Request) {
        w.WriteHeader(http.StatusNoContent)
    })
    GHmux.HandleFunc("/user/repository_invitations/2", func(w http.ResponseWriter, r *http.Request) {
        w.WriteHeader(http.StatusNoContent)
    })
    GHmux.HandleFunc("/repos/"+owner+"/"+repo+"/contents/"+path, func(w http.ResponseWriter, r *http.Request) {
        w.WriteHeader(http.StatusNoContent)
    })
    GHmux.HandleFunc("/repos/"+owner+"/"+repo+"/hooks", func(w http.ResponseWriter, r *http.Request) {
        fmt.Fprint(w, `{"id":1}`)
    })

    fakeBK := func() bbBuildkiteApi.BBbuildKiteApi {
        return bbBuildkiteApi.BBbuildKiteApi{
            &bb_buildkite.BBBuildkite{Client: BKcli}, nil,
        }
    }

    fakeGH := func() bbGithubApi.BBGithubAPi {
        return bbGithubApi.BBGithubAPi{
            GhCli: GHcli,
        }
    }

    githubDataJson := `    "token": "___GITHUB_TOKEN____",
        "owner": "%v",
        "repository": "%v",
        "invitedUserToken": "___INVITED_GITHUB_USER_TOKEN____",`
    jsonInvitedUserName := `    "invitedUserName": "%v",`

    jsonEmptyGithubBuildkite := `{
      "buildKiteData": {
            %v
       },
      "githubData": {
            %v
            %v
       "end_of":"line"
       }
    }`
    noInvUserName := fmt.Sprintf(jsonEmptyGithubBuildkite, "", githubDataJson, "")
    wrongUserName := fmt.Sprintf(jsonEmptyGithubBuildkite, "", fmt.Sprintf(githubDataJson,owner,repo), fmt.Sprintf(jsonInvitedUserName,"wrong_user"))

    jsonAsStr := `{
  "buildKiteData": {
    "token": "___BUILDKITE_TOKEN____",
    "organization": "%v",
    "pipelineConfig": {
      "name": "TestAddPipeLineViaREST",
      "repository": "git@bitbucket.org:blackbeltdevops/nexus-reposize-plugin.git",
      "steps": [
        {
          "type": "script",
          "name": "Build :hammer:",
          "command": "buildkite-agent pipeline upload"
        }
      ]
    }
  },
  "githubData": {
    "token": "___GITHUB_TOKEN____",
    "owner": "%v",
    "repository": "%v",
    "invitedUserToken": "___INVITED_GITHUB_USER_TOKEN____",
    "invitedUserName": "%v",
    "filePath": "%v"
  }
}`
    jsonAsStr = fmt.Sprintf(jsonAsStr, org, owner, repo, user, path)

    successFakeW := httptest.NewRecorder()
    successFakeReq := req("POST", nopCloser{bytes.NewBufferString(jsonAsStr)})

    failEmptyJsonFakeW := httptest.NewRecorder()
    failEmptyJsonlFakeReq := req("POST", nopCloser{bytes.NewBufferString("")})

    failFakeW := httptest.NewRecorder()
    failFakeReq := req("POST", nopCloser{bytes.NewBufferString(`{"fefef":"fafaf"}`)})

    failNoUserNameFakeW := httptest.NewRecorder()
    failNoUserNameFakeReq := req("POST", nopCloser{bytes.NewBufferString(noInvUserName)})

    wrongUserNameFakeW := httptest.NewRecorder()
    wrongUserNameFakeReq := req("POST", nopCloser{bytes.NewBufferString(wrongUserName)})

    type fields struct {
        buildKiteApi         bbBuildkiteApi.BBbuildKiteApi
        githubAPi            bbGithubApi.BBGithubAPi
        invitedUserGithubApi bbGithubApi.BBGithubAPi
    }
    type args struct {
        w http.ResponseWriter
        r *http.Request
    }
    tests := []struct {
        name   string
        fields fields
        args   args
    }{
        {"success", fields{fakeBK(), fakeGH(), fakeGH()},
           args{successFakeW, successFakeReq}},
        {"failEmptyJson", fields{fakeBK(), fakeGH(), fakeGH()},
           args{failEmptyJsonFakeW, failEmptyJsonlFakeReq}},
        {"fail", fields{fakeBK(), fakeGH(), fakeGH()},
           args{failFakeW, failFakeReq}},
        {"failNoUserName", fields{fakeBK(), fakeGH(), fakeGH()},
           args{failNoUserNameFakeW, failNoUserNameFakeReq}},
        {"wrongUserName", fields{fakeBK(), fakeGH(), fakeGH()},
            args{wrongUserNameFakeW, wrongUserNameFakeReq}},
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            bPI := &bbProjInitApi{
                buildKiteApi:         tt.fields.buildKiteApi,
                githubAPi:            tt.fields.githubAPi,
                invitedUserGithubApi: tt.fields.invitedUserGithubApi,
            }
            bPI.Init(tt.args.w, tt.args.r)
            fmt.Println(tt.args.w)
            if tt.name == "success" {
                body := strings.TrimSuffix(successFakeW.Body.String(), "\n")
                assert.Equal(t, 200, successFakeW.Code)
                assert.Equal(t, `{"createFile":"204 No Content","invite":"204 No Content","acceptInvite":"204 No Content","createPipeline":"200 OK","createHook":"200 OK"}`, body)
            }
            if tt.name == "fail" {
                body := strings.TrimSuffix(failFakeW.Body.String(), "\n")
                assert.Equal(t, 400, failFakeW.Code)
                assert.Equal(t, `{"jsonError":"projInitData was empty "}`, body)
            }
            if tt.name == "failNoUserName" {
                body := strings.TrimSuffix(failNoUserNameFakeW.Body.String(), "\n")
                assert.Equal(t, 400, failNoUserNameFakeW.Code)
                assert.Equal(t, `{"inviteError": "parse repos/%v/%v/collaborators/: invalid URL escape "%v/""}`, body)
            }
            if tt.name == "wrongUserName" {
                body := strings.TrimSuffix(wrongUserNameFakeW.Body.String(), "\n")
                assert.Equal(t, 404, wrongUserNameFakeW.Code)
                assert.Regexp(t,`.*?"inviteError": "PUT .*?/api-v3/repos/BlackBeltTechnology/ci-integrator/collaborators/wrong_user: 404  .*?`,body)
            }
            if tt.name == "failEmptyJson" {
                body := strings.TrimSuffix(failEmptyJsonFakeW.Body.String(), "\n")
                assert.Equal(t, 400, failEmptyJsonFakeW.Code)
                assert.Equal(t, `{"jsonError":"EOF"}`, body)
            }
        })
    }
}
