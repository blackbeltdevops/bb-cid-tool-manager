package bbProjInit

import (
    "bitbucket.org/blackbeltdevops/bb-cid-tool-manager/lib"
    "net/http"
    "bitbucket.org/blackbeltdevops/bb-cid-tool-manager/bbBuildkiteApi"
    "bitbucket.org/blackbeltdevops/bb-cid-tool-manager/bbGithubApi"
    "github.com/buildkite/go-buildkite/buildkite"
    "encoding/json"
    "fmt"
    "bitbucket.org/blackbeltdevops/bb-github-api/bb-github"
    "github.com/google/go-github/github"
    "bitbucket.org/blackbeltdevops/bb-github-api/api"
    "errors"
)

var (
    bbBuildKiteGlobal = bbProjInitApi{}
)

type bbProjInitApi struct {
    writer               http.ResponseWriter
    buildKiteApi         bbBuildkiteApi.BBbuildKiteApi
    githubAPi            bbGithubApi.BBGithubAPi
    invitedUserGithubApi bbGithubApi.BBGithubAPi
    jsonResponse struct {
        CreateFile     string `json:"createFile"`
        Invite         string `json:"invite"`
        AcceptInvite   string `json:"acceptInvite"`
        CreatePipeline string `json:"createPipeline"`
        CreateHook     string `json:"createHook"`
    }
}

func (bPI *bbProjInitApi) makeContextAndClient(tokenGH, tokenBK, invitedUserToken string) {
    if (bPI.githubAPi == bbGithubApi.BBGithubAPi{} && len(tokenGH) > 0) {
        bPI.githubAPi.MakeContextAndClient(tokenGH)
    }
    if (bPI.invitedUserGithubApi == bbGithubApi.BBGithubAPi{} && len(invitedUserToken) > 0) {
        bPI.invitedUserGithubApi.MakeContextAndClient(invitedUserToken)
    }
    if (bPI.buildKiteApi == bbBuildkiteApi.BBbuildKiteApi{} && len(tokenBK) > 0) {
        bPI.buildKiteApi.MakeContextAndClient(tokenBK)
    }
}

func GetFuncRouter() []lib.FunctionRouter {
    return [] lib.FunctionRouter{
        lib.FunctionRouter{RouteName: "/Init", Func: bbBuildKiteGlobal.Init}.Gen(),
        lib.FunctionRouter{RouteName: "/CreatePipelineAndHook", Func: bbBuildKiteGlobal.CreatePipelineAndHook}.Gen(),
    }
}

type projInitData struct {
    BuildKiteData struct {
        Token          string                    `json:"token,omitempty"`
        Organization   string                    `json:"organization,omitempty"`
        PipelineConfig *buildkite.CreatePipeline `json:"pipelineConfig,omitempty"`
    } `json:"buildKiteData,omitempty"`
    GithubData struct {
        Token            string       `json:"token,omitempty"`
        Owner            string       `json:"owner,omitempty"`
        Repository       string       `json:"repository,omitempty"`
        InvitedUserToken string       `json:"invitedUserToken,omitempty"`
        InvitedUserName  string       `json:"invitedUserName,omitempty"`
        FilePath         string       `json:"filePath,omitempty"`
        RepoContentOpts  string       `json:"repoContentOpts,omitempty"`
        Hook             *github.Hook `json:"hook,omitempty"`
    } `json:"githubData,omitempty"`
}

func (bPI *bbProjInitApi) readAndAcceptInvitations(invitedUser *github.Client) error {
    invitedUserBBGh := bb_github.NewClientWithGithubClient(invitedUser)

    invitationRepos, resp, err := invitedUserBBGh.Invitations()
    if err != nil {
        bPI.writer.WriteHeader(resp.StatusCode)
        fmt.Fprintf(bPI.writer, `{"invitationsEerror": "%v"}`, err.Error())
        return errors.New(err.Error())
    }

    resp, err = api.InvErrorEvaluation(invitedUserBBGh.AcceptAllInvitations(invitationRepos, resp, err))
    if err != nil {
        bPI.writer.WriteHeader(resp.StatusCode)
        fmt.Fprintf(bPI.writer, `{"acceptAllInvitationsError": "%v"}`, err.Error())
        return errors.New(err.Error())
    }

    bPI.jsonResponse.AcceptInvite = resp.Status
    return nil
}
func (bPI *bbProjInitApi) createFile(BBGh bb_github.BBGithub, pid projInitData) error {
    owner, repo, path := api.FileCreate(
        pid.GithubData.Owner,
        pid.GithubData.Repository,
        pid.GithubData.FilePath,
    )
    _, resp, err := BBGh.AddDefaultBuildkitePipeline(
        owner, repo, path, api.GetOpts(pid.GithubData.RepoContentOpts),
    )
    if err != nil {
        bPI.writer.WriteHeader(resp.StatusCode)
        fmt.Fprintf(bPI.writer, `{"createFileError": "%v"}`, err.Error())
        return errors.New(err.Error())
    }
    bPI.jsonResponse.CreateFile = resp.Status
    return nil
}
func (bPI *bbProjInitApi) invite(BBGh bb_github.BBGithub, pid projInitData) error {

    resp, err := BBGh.Invite(api.Invite(pid.GithubData.Owner,
        pid.GithubData.Repository,
        pid.GithubData.InvitedUserName,
    )).Push()

    if err != nil {
        if resp != nil {
            bPI.writer.WriteHeader(resp.StatusCode)
        } else {
            bPI.writer.WriteHeader(400)
        }
        fmt.Fprintf(bPI.writer, `{"inviteError": "%v"}`, err.Error())
        return err
    }
    bPI.jsonResponse.Invite = resp.Status

    return err
}
func (bPI *bbProjInitApi) createPipeline(BBGh bb_github.BBGithub, pid projInitData) (string, error) {
    pipeline, resp, err := bPI.buildKiteApi.BuildKiteCli.ConfigV2(pid.BuildKiteData.PipelineConfig).PipelinesCreate(pid.BuildKiteData.Organization)

    if err != nil {
        bPI.writer.WriteHeader(resp.StatusCode)
        fmt.Fprintf(bPI.writer, `{"pipelinesCreateError": "%v"}`, err.Error())
        return "", err
    }

    if pipeline.Provider == nil || pipeline.Provider.WebhookURL == nil {
        bPI.writer.WriteHeader(400)
        fmt.Fprintf(bPI.writer, `{"webhookUrlEmptyError": "%v"}`, "empty webhook url found")
        return "", err
    }

    bPI.jsonResponse.CreatePipeline = resp.Status
    return *pipeline.Provider.WebhookURL, nil
}
func (bPI *bbProjInitApi) createHook(BBGh bb_github.BBGithub, pid projInitData, webhook string) error {
    hPay := api.CreateHook(pid.GithubData.Owner,
        pid.GithubData.Repository,
        pid.GithubData.Hook,
    )

    hPay.Hook.Config = map[string]interface{}{"url": webhook, "content_type": "json"}

    _, resp, err := BBGh.CreateWebHook(hPay)
    if err != nil {
        bPI.writer.WriteHeader(resp.StatusCode)
        fmt.Fprintf(bPI.writer, `{"createHookError": "%v"}`, err.Error())
    }
    bPI.jsonResponse.CreateHook = resp.Status
    return nil
}

func (bPI *bbProjInitApi) hasError(err error) bool {
    if err != nil {
        //json.NewEncoder(bPI.writer).Encode(bPI.jsonResponse)
        return true
    }
    return false
}

func (bPI *bbProjInitApi) Init(w http.ResponseWriter, r *http.Request) {
    bPI.writer = w
    w.Header().Set("Content-Type", "application/json")

    pid := projInitData{}
    err := json.NewDecoder(r.Body).Decode(&pid)
    if (pid == projInitData{} || err != nil) {
        w.WriteHeader(400)
        if err == nil {
            err = errors.New("projInitData was empty ")
        }
        fmt.Fprintf(w, `{"jsonError":"%v"}`, err.Error())
        return
    }

    bPI.makeContextAndClient(pid.GithubData.Token, pid.BuildKiteData.Token, pid.GithubData.InvitedUserToken)
    BBGh := bb_github.NewClientWithGithubClient(bPI.githubAPi.GhCli)

    if bPI.hasError(bPI.invite(BBGh, pid)) {
        return
    }

    err = bPI.readAndAcceptInvitations(bPI.invitedUserGithubApi.GhCli )
    if err != nil {
        return
    }

    err = bPI.createFile(BBGh, pid)
    if err != nil {
        return
    }

    webhook, err := bPI.createPipeline(BBGh, pid)
    if err != nil {
        return
    }

    err = bPI.createHook(BBGh, pid, webhook)
    if err != nil {
        return
    }

    json.NewEncoder(w).Encode(bPI.jsonResponse)
    w.WriteHeader(200)
}
func (bPI *bbProjInitApi) CreatePipelineAndHook(w http.ResponseWriter, r *http.Request) {
    bPI.writer = w
    w.Header().Set("Content-Type", "application/json")

    pid := projInitData{}
    err := json.NewDecoder(r.Body).Decode(&pid)
    if (pid == projInitData{} || err != nil) {
        w.WriteHeader(400)
        if err == nil {
            err = errors.New("projInitData was empty ")
        }
        fmt.Fprintf(w, `{"jsonError":"%v"}`, err.Error())
        return
    }

    bPI.makeContextAndClient(pid.GithubData.Token, pid.BuildKiteData.Token, "")
    BBGh := bb_github.NewClientWithGithubClient(bPI.githubAPi.GhCli)

    webhook, err := bPI.createPipeline(BBGh, pid)
    if err != nil {
        return
    }

    err = bPI.createHook(BBGh, pid, webhook)
    if err != nil {
        return
    }

    w.WriteHeader(200)
}
