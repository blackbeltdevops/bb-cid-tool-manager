package bbBuildkiteApi

import (
    "bitbucket.org/blackbeltdevops/bb-cid-tool-manager/lib"
    "net/http"
    "github.com/buildkite/go-buildkite/buildkite"
    "bitbucket.org/blackbeltdevops/bb-buildkite-api/bb-buildkite"
    "encoding/json"
    "log"
)

var (
    bbBuildKiteGlobal = BBbuildKiteApi{}
)

type BBbuildKiteApi struct {
    BuildKiteCli *bb_buildkite.BBBuildkite
    E            error
}

type RepoAndHook struct {
    PipelineName string
    WebHookUrl   string
}

type BuildKiteApiFunc struct {
    Api func(t lib.Token, params string) (*buildkite.Response, error)
}

type Organisation struct {
    Org string `json:"organization"`
}

type createData struct {
    lib.DataFlowManager
    BuildKiteApiFunc
    Create func(BBBk *bb_buildkite.BBBuildkite, params string) (*buildkite.Response, error)
}

type listData struct {
    lib.DataFlowManager
    BuildKiteApiFunc
    List func(BBBk *bb_buildkite.BBBuildkite, params string) (*buildkite.Response, error)
}

func GetFuncRouter() []lib.FunctionRouter {
    return [] lib.FunctionRouter{
        lib.FunctionRouter{RouteName: "/CreatePipeline", Func: bbBuildKiteGlobal.CreatePipeline}.Gen(),
        lib.FunctionRouter{RouteName: "/ListAllPipeline", Func: bbBuildKiteGlobal.CreatePipeline}.Gen(),
    }
}

func createApi(b *BBbuildKiteApi) (tmp *BBbuildKiteApi) {
    if (*b == BBbuildKiteApi{}) {
        tmp = new(BBbuildKiteApi)
        log.Println("Session id:", lib.MD5(tmp))
        return
    }
    return b
}

func (b *BBbuildKiteApi) deleteApi() {
    log.Println("Remove session:", lib.MD5(b))
    *b = BBbuildKiteApi{}
}

func (b *BBbuildKiteApi) MakeContextAndClient(token string) {
    b.makeContextAndClient(token)
}

func (b *BBbuildKiteApi) makeContextAndClient(token string) {
    if b.BuildKiteCli == nil {
        config, err := buildkite.NewTokenConfig(token, false)
        if err == nil {
            b.BuildKiteCli = &bb_buildkite.BBBuildkite{
                Client: buildkite.NewClient(config.Client()),
            }
        }
        b.E = err
    }
}

func (b *BBbuildKiteApi) reqHan(d *lib.DataFlowManager, dApi BuildKiteApiFunc) (statCode int) {
    statCode, t, params := lib.RequestHandler(d)
    if statCode >= 200 && statCode < 300 {
        resp, err := dApi.Api(t, params)
        if resp.StatusCode < 200 || resp.StatusCode >= 300 {
            d.Msg = d.FunctionName + " failed, reason: " + err.Error()
        }
        statCode = resp.StatusCode
    }
    b.deleteApi()
    d.W.WriteHeader(statCode)
    return
}

func (b *BBbuildKiteApi) CliEmptyError() (*buildkite.Response, error) {
    if b.E != nil {
        return &buildkite.Response{
            Response: &http.Response{StatusCode: 400},
        }, b.E
    }
    return nil, nil
}

func (b *BBbuildKiteApi) create(cD *createData) {
    cD.Api = func(t lib.Token, params string) (*buildkite.Response, error) {
        b.makeContextAndClient(t.Token)
        if resp, err := b.CliEmptyError(); err != nil {
            return resp, err
        }
        return cD.Create(b.BuildKiteCli, params)
    }
    statCode := b.reqHan(&cD.DataFlowManager, cD.BuildKiteApiFunc)
    if statCode >= 200 && statCode < 300 {
        return
    }
    hMsg := lib.HelperMsg{W: cD.W, Helper: ` {
        "msg" : "%v",
        "url" : "%v",
        "JsonToBeSent" : {
            "token"          : "< Token for github api >",
            "organization"   : "< Organization name >",
            "pipelineConfig" : {
                "name"          : "< Pipeline name >",
                "repository"    : "< Git repo url>",
                "steps": [
                    {
                        "type": "script",
                        "name": "Build :hammer:",
                        "command": "buildkite-agent pipeline upload"
                    }
                ]
            }
        }
    } `, Msg: cD.Msg, FunctionName: cD.FunctionName}

    lib.HelperMessage(&hMsg, GetFuncRouter)
}

func (b *BBbuildKiteApi) list(cD *listData) {
    cD.Api = func(t lib.Token, params string) (*buildkite.Response, error) {
        b.makeContextAndClient(t.Token)
        if resp, err := b.CliEmptyError(); err != nil {
            return resp, err
        }
        return cD.List(b.BuildKiteCli, params)
    }
    statCode := b.reqHan(&cD.DataFlowManager, cD.BuildKiteApiFunc)
    if statCode >= 200 && statCode < 300 {
        return
    }
    hMsg := lib.HelperMsg{W: cD.W, Helper: ` {
        "msg" : "%v",
        "url" : "%v",
        "JsonToBeSent" : {
            "token"          : "< Token for github api >",
            "organization"   : "< Organization name >"
        }
    } `, Msg: cD.Msg, FunctionName: cD.FunctionName}

    lib.HelperMessage(&hMsg, GetFuncRouter)
}

func (b *BBbuildKiteApi) CreatePipeline(w http.ResponseWriter, r *http.Request) {
    b = createApi(b)
    cD := createData{lib.DataFlowManager{FunctionName: "CreatePipeline", Method: lib.POST, W: w, R: r,}.FillMsg(), BuildKiteApiFunc{}, nil}
    cD.Create = func(BBBk *bb_buildkite.BBBuildkite, params string) (resp *buildkite.Response, err error) {
        org := Organisation{}
        json.Unmarshal([]byte(params), &org)
        pipeline, resp, err := BBBk.Config(params).PipelinesCreate(org.Org)
        if err == nil {
            json.NewEncoder(w).Encode(pipeline)
        }
        return
    }
    b.create(&cD)
}

func (b *BBbuildKiteApi) ListPipeline(w http.ResponseWriter, r *http.Request) {
    b = createApi(b)
    cD := listData{lib.DataFlowManager{FunctionName: "ListPipeline", Method: lib.POST, W: w, R: r,}.FillMsg(), BuildKiteApiFunc{}, nil}
    cD.List = func(BBBk *bb_buildkite.BBBuildkite, params string) (resp *buildkite.Response, err error) {
        org := Organisation{}
        json.Unmarshal([]byte(params), &org)
        pipe, resp, err := BBBk.ListWebHook(org.Org)

        var rah []RepoAndHook
        for _, onePipe := range pipe {
            if onePipe.Provider != nil && onePipe.Provider.WebhookURL != nil {
                rah = append(rah, RepoAndHook{*onePipe.Repository, *onePipe.Provider.WebhookURL})
            }
        }
        err = json.NewEncoder(w).Encode(rah)
        return
    }
    b.list(&cD)
}
