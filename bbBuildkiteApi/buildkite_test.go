package bbBuildkiteApi

import (
    "bytes"
    "errors"
    "fmt"
    "io"
    "net/http"
    "net/http/httptest"
    "net/url"
    "reflect"
    "testing"

    "bitbucket.org/blackbeltdevops/bb-buildkite-api/bb-buildkite"
    "bitbucket.org/blackbeltdevops/bb-cid-tool-manager/lib"
    "github.com/buildkite/go-buildkite/buildkite"
    "github.com/stretchr/testify/assert"
    "encoding/json"
)

func setup() (client *buildkite.Client, mux *http.ServeMux, serverURL string, teardown func()) {
    mux = http.NewServeMux()
    server := httptest.NewServer(mux)
    client = buildkite.NewClient(http.DefaultClient)
    sUrl, _ := url.Parse(server.URL)
    client.BaseURL = sUrl
    return client, mux, server.URL, server.Close
}

type nopCloser struct {
    io.Reader
}

func (nopCloser) Close() error { return nil }

var bEmpty = nopCloser{bytes.NewBufferString("")}

func req(method string, body nopCloser) *http.Request {
    return &http.Request{
        Method: method,
        URL:    &url.URL{Host: "/Invite/Push"},
        Body:   body,
    }
}

func TestBBBuildkiteApi_makeContextAndClient(t *testing.T) {
    emptyBBbuildKite := BBbuildKiteApi{}
    type args struct {
        token string
    }
    tests := []struct {
        name string
        b    *BBbuildKiteApi
        args args
    }{
        {"empty", &emptyBBbuildKite, args{"token_asdfasdfasdf"}},
        {"noToken", &emptyBBbuildKite, args{""}},
    }
    for _, tt := range tests {
        assert.Nil(t, tt.b.BuildKiteCli, "BuildKiteCli not nil")

        t.Run(tt.name, func(t *testing.T) {
            tt.b.makeContextAndClient(tt.args.token)
        })
        if tt.name == "empty" {
            assert.NotNil(t, tt.b.BuildKiteCli, "BuildKiteCli nil")
            emptyBBbuildKite = BBbuildKiteApi{}
        }
        if tt.name == "noToken" {
            assert.Nil(t, tt.b.BuildKiteCli, "BuildKiteCli nil")
            assert.NotNil(t, tt.b.E, "BuildKite error not nil")
            emptyBBbuildKite = BBbuildKiteApi{}
        }
    }
}

func TestBBbuildKiteApi_cliEmptyError(t *testing.T) {
    f := func() (*buildkite.Response, error) {
        return &buildkite.Response{
            Response: &http.Response{StatusCode: 400},
        }, errors.New("FAIL")
    }

    fakeResp, err := f()

    type fields struct {
        BuildKiteCli *bb_buildkite.BBBuildkite
        e            error
    }
    tests := []struct {
        name    string
        fields  fields
        want    *buildkite.Response
        wantErr bool
    }{
        {"successNoError", fields{nil, nil}, nil, false},
        {"successErrorHappened", fields{nil, err}, fakeResp, true},
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            b := &BBbuildKiteApi{
                BuildKiteCli: tt.fields.BuildKiteCli,
                E:            tt.fields.e,
            }
            got, err := b.CliEmptyError()
            if (err != nil) != tt.wantErr {
                t.Errorf("BBbuildKiteApi.CliEmptyError() error = %v, wantErr %v", err, tt.wantErr)
                return
            }
            if !reflect.DeepEqual(got, tt.want) {
                t.Errorf("BBbuildKiteApi.CliEmptyError() = %v, want %v", got, tt.want)
            }
        })
    }
}

func TestGetFuncRouter(t *testing.T) {
    want := []lib.FunctionRouter{
        {RouteName: "/CreatePipeline", GeneratedFuncName: "CreatePipeline"},
    }
    tests := []struct {
        name string
        want []lib.FunctionRouter
    }{
        {"success", want},
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            got := GetFuncRouter()
            if !reflect.DeepEqual(got[0].RouteName, tt.want[0].RouteName) {
                t.Errorf("GetFuncRouter() = %v, want %v", got, tt.want)
            }
            if !reflect.DeepEqual(got[0].GeneratedFuncName, tt.want[0].GeneratedFuncName) {
                t.Errorf("GetFuncRouter() = %v, want %v", got, tt.want)
            }
        })
    }
}

func TestBBBuildkiteApi_reqHan(t *testing.T) {
    fakeDFM := func(dfmMethod string, dfmRreqMethod string, body string) lib.DataFlowManager {
        return lib.DataFlowManager{FunctionName: "TestFuncName", W: httptest.NewRecorder(), Method: dfmMethod,
            R: req(dfmRreqMethod, nopCloser{bytes.NewBufferString(body)}),
        }
    }
    dPutFullBody := fakeDFM("PUT", lib.POST, `{"sasa":"fefe"}`)
    dPostFullBody := fakeDFM(lib.POST, lib.POST, `{"sasa":"fefe"}`)
    dPostFullBodyApiFail := fakeDFM(lib.POST, lib.POST, `{"sasa":"fefe"}`)

    fakeBKApiFunc := func(statCode int) BuildKiteApiFunc {
        return BuildKiteApiFunc{
            Api: func(t lib.Token, params string) (*buildkite.Response, error) {
                return &buildkite.Response{
                    Response: &http.Response{StatusCode: statCode},
                }, errors.New("RandomError")
            },
        }
    }
    type fields struct {
        BuildKiteCli *bb_buildkite.BBBuildkite
    }
    type args struct {
        d    *lib.DataFlowManager
        dApi BuildKiteApiFunc
    }
    tests := []struct {
        name   string
        fields fields
        args   args
        want   int
    }{
        {"FailCuzPut", fields{nil}, args{&dPutFullBody, BuildKiteApiFunc{}}, 400},
        {"successPost", fields{nil}, args{&dPostFullBody, fakeBKApiFunc(200)}, 200},
        {"FailWithMessage", fields{nil}, args{&dPostFullBodyApiFail, fakeBKApiFunc(300)}, 300},
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            b := &BBbuildKiteApi{
                BuildKiteCli: tt.fields.BuildKiteCli,
            }
            got := b.reqHan(tt.args.d, tt.args.dApi)

            if got != tt.want {
                t.Errorf("reqHan() = %v, want %v", got, tt.want)
            }
            if tt.name == "FailWithMessage" {
                assert.Equal(t, tt.args.d.Msg, "TestFuncName failed, reason: RandomError")
            }
        })

    }
}

func TestBBBuildkiteApi_CreatePipeline(t *testing.T) {
    cli, mux, _, teardown := setup()
    defer teardown()

    testOrg := "BlackBeltTechnology"

    mux.HandleFunc("/v2/organizations/"+testOrg+"/pipelines",
        func(w http.ResponseWriter, r *http.Request) {
            w.WriteHeader(http.StatusNoContent)
            fmt.Fprint(w, `{
						"name":"my-great-pipeline",
						"repository":"my-great-repo",
						"steps": [
							{
								"type": "script",
								"name": "Build :package:",
								"command": "script/release.sh"
							}
						]
					}`)
        },
    )

    jsonAsStr := ` {
        "token"          : "< Token for github api >",
        "organization"   : "%v", 
        "pipelineConfig" : {
            "name":"TestAddPipeLineViaREST",
            "repository":"git@bitbucket.org:blackbeltdevops/nexus-reposize-plugin.git",
            "steps": [
                {
                    "type": "script",
                    "name": "Build :hammer:",
                    "command": "buildkite-agent pipeline upload"
                }
            ]
        }
    }`
    jsonAsStr = fmt.Sprintf(jsonAsStr, testOrg)

    fakeB := BBbuildKiteApi{
       &bb_buildkite.BBBuildkite{Client: cli}, nil,
    }
    configNotFoundB := BBbuildKiteApi{
        &bb_buildkite.BBBuildkite{Client: cli}, nil,
    }
    fakeBFail := BBbuildKiteApi{
       &bb_buildkite.BBBuildkite{Client: cli}, errors.New("FAILFAIL"),
    }
    configNotFoundW := httptest.NewRecorder()
    configNotFoundReq := req("POST", nopCloser{bytes.NewBufferString(`{"json":"json"}`)})
    successFakeW := httptest.NewRecorder()
    successFakeReq := req("POST", nopCloser{bytes.NewBufferString(jsonAsStr)})
    failFakeW := httptest.NewRecorder()
    failFakeReq := req("POST", nopCloser{bytes.NewBufferString(jsonAsStr)})

    type args struct {
        w http.ResponseWriter
        r *http.Request
    }
    tests := []struct {
        name string
        b    *BBbuildKiteApi
        args args
    }{
        {"configNotFound", &configNotFoundB, args{configNotFoundW, configNotFoundReq}},
        {"success", &fakeB, args{successFakeW, successFakeReq}},
        {"fail", &fakeBFail, args{failFakeW, failFakeReq}},
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            tt.b.CreatePipeline(tt.args.w, tt.args.r)

            fmt.Println(tt.args.w)
            if tt.name == "configNotFound" {
                assert.Equal(t, 400, configNotFoundW.Code, "config not 400")
            }
            if tt.name == "success" {
                assert.Equal(t, 204, successFakeW.Code, "not 204")
            }
            if tt.name == "fail" {
                assert.Equal(t, 400, failFakeW.Code, "fail not 400")
            }

        })

    }
}

func TestBBbuildKiteApi_ListPipeline(t *testing.T) {
    cli, mux, _, teardown := setup()
    defer teardown()

    testOrg := "BlackBeltTechnology"

    jsonAsStr := ` {
        "token"          : "afafafafafafafaf",
        "organization"   : "%v"
    }`
    jsonAsStr = fmt.Sprintf(jsonAsStr, testOrg)

    fakeB := BBbuildKiteApi{
        &bb_buildkite.BBBuildkite{Client: cli}, nil,
    }

    emptyB := BBbuildKiteApi{
        &bb_buildkite.BBBuildkite{Client: cli}, nil,
    }

    successFakeW := httptest.NewRecorder()
    emptyFakeW := httptest.NewRecorder()

    successFakeReq := req("POST", nopCloser{bytes.NewBufferString(jsonAsStr)})
    emptyFakeReq := req("POST", nopCloser{bytes.NewBufferString("")})

    mux.HandleFunc("/v2/organizations/"+testOrg+"/pipelines", func(w http.ResponseWriter, r *http.Request) {
        fmt.Fprint(w, `[{"id":"123"},{"id":"1234", "repository": "fafa","provider": { "id": "1234", "webhook_url": "1234" } } ]`)
    })

    type fields struct {
        BuildKiteCli *bb_buildkite.BBBuildkite
        e            error
    }
    type args struct {
        w http.ResponseWriter
        r *http.Request
    }
    tests := []struct {
        name   string
        fields fields
        args   args
    }{
        {"success", fields{fakeB.BuildKiteCli, fakeB.E}, args{successFakeW, successFakeReq}},
        {"empty", fields{emptyB.BuildKiteCli, emptyB.E}, args{emptyFakeW, emptyFakeReq}},
    }
    var rah []RepoAndHook
    wantEmptyRah := []RepoAndHook{}
    wantRah := []RepoAndHook{{"fafa", "1234"}}

    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            b := &BBbuildKiteApi{
                BuildKiteCli: tt.fields.BuildKiteCli,
                E:            tt.fields.e,
            }
            b.ListPipeline(tt.args.w, tt.args.r)
            if tt.name == "success" {
                            fmt.Println(tt.args.w)
                            fmt.Println(tt.args.r)

                json.Unmarshal(successFakeW.Body.Bytes(), &rah)
                assert.Equal(t, wantRah, rah)
                assert.Equal(t, 200, successFakeW.Code)

            }
            if tt.name == "empty" {
                json.Unmarshal(emptyFakeW.Body.Bytes(), &rah)
                assert.Equal(t, wantEmptyRah, rah)
                assert.Equal(t, 400, emptyFakeW.Code)
            }
            rah = []RepoAndHook{}
        })
    }
}
