ARG SRC_DIR=src/bitbucket.org/blackbeltdevops/bb-cid-tool-manager

# build stage
FROM iron/go:dev AS build-env
ARG SRC_DIR
WORKDIR /go
COPY .  $SRC_DIR
RUN cd  $SRC_DIR  && \
    go get -t -d && go get github.com/stretchr/testify && \
    go test -v ./... && \
    CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main

# final stage#
FROM alpine
ARG SRC_DIR
WORKDIR /app
COPY --from=build-env /go/$SRC_DIR/main /app/
RUN apk --update add ca-certificates
ENTRYPOINT ["/app/main"]
