# USEFUL STUFF HERE

for i in $(go list ./...); do go test -c $i -o test/$i ; done

for i in $(go list ./...); do CGO_ENABLED=0 GOOS=linux go test -covermode=atomic -a -c -installsuffix cgo $i -o test/$i ; done


for i in $(find . -type f -executable -exec sh -c "file -i '{}' | grep -q 'x-executable; charset=binary'" \; -print
); do $i -test.v ; done
