package lib

import (
    "net/http"
    "reflect"
    "testing"
    "net/url"
    "bytes"
    "net/http/httptest"
    "fmt"
    "github.com/stretchr/testify/assert"
    "io"
)

var bEmpty = nopCloser{bytes.NewBufferString("")}

func req(method string, body nopCloser) *http.Request {
    return &http.Request{
        Method: method,
        URL:    &url.URL{Host: "/Invite/Push"},
        Body:   body,
    }
}

type nopCloser struct {
    io.Reader
}

func (nopCloser) Close() error { return nil }


func TestDataFlowManager_FillMsg(t *testing.T) {
    tests := []struct {
        name string
        d    DataFlowManager
        want DataFlowManager
    }{
        {"success", DataFlowManager{Method: "SasaFunc"}, DataFlowManager{Method: "SasaFunc", Msg: "Not a SasaFunc request or Request body is empty"}},
        {"successAgainEmpty", DataFlowManager{Method: ""}, DataFlowManager{Method: "", Msg: "Not a  request or Request body is empty"}},
        {"noMethod", DataFlowManager{}, DataFlowManager{Msg: "Not a  request or Request body is empty"}},
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            if got := tt.d.FillMsg(); !reflect.DeepEqual(got, tt.want) {
                t.Errorf("DataFlowManager.FillMsg() = %v, want %v", got, tt.want)
            }
        })
    }
}



func TestRequestMethod(t *testing.T) {

    rPost := req(POST, bEmpty)
    rGet := req(GET, bEmpty)
    rPut := req("PUT", bEmpty)

    //err := errors.New("")

    type args struct {
        r *http.Request
    }
    tests := []struct {
        name    string
        args    args
        want    string
        want1   string
        wantErr bool
    }{
        {"successPost", args{rPost}, POST, "", false},
        {"successGet", args{rGet}, GET, "", false},
        {"successPut", args{rPut}, "", "", true},
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            got, got1, err := RequestMethod(tt.args.r)
            if (err != nil) != tt.wantErr {
                t.Errorf("RequestMethod() error = %v, wantErr %v", err, tt.wantErr)
                return
            }
            if got != tt.want {
                t.Errorf("RequestMethod() got = %v, want %v", got, tt.want)
            }
            if got1 != tt.want1 {
                t.Errorf("RequestMethod() got1 = %v, want %v", got1, tt.want1)
            }
        })
    }
}

func TestRequestHandler(t *testing.T) {
    dPutFullBody := DataFlowManager{W: httptest.NewRecorder(), Method: "PUT",
        R: req(POST, nopCloser{bytes.NewBufferString(`{"sasa":"fefe"}`)}),
    }
    dPostEmptyBody := DataFlowManager{W: httptest.NewRecorder(), Method: POST,
        R: req(POST, nopCloser{bytes.NewBufferString(`{}`)}),
    }
    dPutFullBodyError := DataFlowManager{W: httptest.NewRecorder(), Method: "PUT",
        R: req("PUT", nopCloser{bytes.NewBufferString(`{"sasa":"fefe"}`)}),
    }
    dPostFullBody := DataFlowManager{W: httptest.NewRecorder(), Method: POST,
        R: req(POST, nopCloser{bytes.NewBufferString(`{"sasa":"fefe"}`)}),
    }
    dPostFullBodyWrongJson := DataFlowManager{W: httptest.NewRecorder(), Method: POST,
        R: req(POST, nopCloser{bytes.NewBufferString(`{{{{{{{"sasa":"fefe"}`)}),
    }

    type args struct {
        d *DataFlowManager
    }
    tests := []struct {
        name  string
        args  args
        want  int
        want1 Token
        want2 string
    }{
        {"WrongMethod", args{&dPutFullBody}, 400, Token{}, ""},
        {"EmptyBody", args{&dPostEmptyBody}, 400, Token{}, ""},
        {"Error", args{&dPutFullBodyError}, 400, Token{}, ""},
        {"JsonError", args{&dPostFullBodyWrongJson}, 400, Token{}, ""},
        {"Json", args{&dPostFullBody}, 200, Token{}, `{"sasa":"fefe"}`},
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            got, got1, got2 := RequestHandler(tt.args.d)
            if got != tt.want {
                t.Errorf("RequestHandler() got = %v, want %v", got, tt.want)
            }
            if !reflect.DeepEqual(got1, tt.want1) {
                t.Errorf("RequestHandler() got1 = %v, want %v", got1, tt.want1)
            }
            if got2 != tt.want2 {
                t.Errorf("RequestHandler() got2 = %v, want %v", got2, tt.want2)
            }
        })
    }
}

func TestHelperMessage(t *testing.T) {
    want := httptest.NewRecorder()
    fmt.Fprint(want, "fafa_kekekek_sasa")
    hMsg := HelperMsg{W: httptest.NewRecorder(), Helper: `fafa_%v%v_sasa`, Msg: "kekekek", FunctionName: "Faaaaaa"}
    gF := func() []FunctionRouter {
        return []FunctionRouter{{"routeName", func(http.ResponseWriter, *http.Request) {}, "GeneratedFuncName"}}
    }
    type args struct {
        hMsg *HelperMsg
        gF   func() []FunctionRouter
    }
    tests := []struct {
        name string
        args args
    }{
        {"success", args{&hMsg, gF}},
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            HelperMessage(tt.args.hMsg, tt.args.gF)
        })
        if ! assert.Equal(t, hMsg.W, want) {
            t.Errorf("RequestHandler() got2 = %v, want %v", hMsg.W, want)

        }
    }
}

func fafa(http.ResponseWriter, *http.Request) {

}

func TestFunctionRouter_Gen(t *testing.T) {

    fR := FunctionRouter{RouteName: "routeName", Func: fafa}
    fRWant := FunctionRouter{"routeName", fafa, "fafa"}
    tests := []struct {
        name string
        f    FunctionRouter
        want FunctionRouter
    }{
        {"success", fR, fRWant},
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            got := tt.f.Gen()
            ok := assert.Equal(t, got.RouteName, fRWant.RouteName) &&
                assert.Equal(t, got.GeneratedFuncName, fRWant.GeneratedFuncName)
            if ! ok {
                t.Errorf("FunctionRouter.Gen() = %v, want %v", got, tt.want)
            }
        })
    }
}

func TestMapKey(t *testing.T) {
    gF := func() []FunctionRouter {
        return []FunctionRouter{{"routeName", func(http.ResponseWriter, *http.Request) {}, "GeneratedFuncName"}}
    }
    type args struct {
        m     []FunctionRouter
        value string
    }
    tests := []struct {
        name    string
        args    args
        wantKey string
    }{
        {"success", args{gF(), "GeneratedFuncName"}, "routeName"},
        {"successEmpty", args{gF(), "Saaaaaaa"}, ""},
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            if gotKey := MapKey(tt.args.m, tt.args.value); gotKey != tt.wantKey {
                t.Errorf("MapKey() = %v, want %v", gotKey, tt.wantKey)
            }
        })
    }
}
