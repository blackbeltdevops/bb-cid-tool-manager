package lib

import (
    "net/http"
    "reflect"
    "runtime"
    "strings"
    "io/ioutil"
    "errors"
    "fmt"
    "encoding/json"
    "crypto/md5"
    "io"
    "bytes"
)

var (
    GET  = "GET"
    POST = "POST"
)

type Token struct {
    Token string `json:"Token"`
}

type BKD struct {
    BuildkiteData struct {
        Token        string `json:"token"`
        Organization string `json:"organization"`
    }
}
type HelperMsg struct {
    W            http.ResponseWriter
    Helper       string
    Msg          string
    FunctionName string
}

type DataFlowManager struct {
    FunctionName string
    Method       string
    Msg          string
    W            http.ResponseWriter
    R            *http.Request
}

type FunctionRouter struct {
    RouteName         string
    Func              func(http.ResponseWriter, *http.Request)
    GeneratedFuncName string
}

func (d DataFlowManager) FillMsg() DataFlowManager {
    d.Msg = "Not a " + d.Method + " request or Request body is empty"
    return d
}

func MD5(i interface{}) string {
    h := md5.New()
    io.WriteString(h, fmt.Sprintf("%p", i))
    return fmt.Sprintf("%x", h.Sum(nil))
}

func RequestMethod(r *http.Request) (string, string, error) {
    switch r.Method {
    case GET:
        return GET, r.URL.Query().Encode(), nil
    case POST:
        defer r.Body.Close()
        body, _ := ioutil.ReadAll(r.Body)
        return POST, string(body), nil
    default:
        return "", "", errors.New("only GET and POST methods are supported")
    }
}

func Request(cli *http.Client, requestType, url, payload, contentType string) (*http.Response, error, string) {
    req, err := http.NewRequest(requestType, url, bytes.NewBuffer([]byte(payload)))
    req.Header.Set("Accept", "application/json")
    req.Header.Set("Content-Type", contentType)
    if cli == nil {
        cli = &http.Client{}
    }
    resp, err := (cli).Do(req)
    defer resp.Body.Close()
    body, _ := ioutil.ReadAll(resp.Body)
    return resp, err, string(body)
}

func RequestHandler(d *DataFlowManager) (int, Token, string) {
    d.W.Header().Set("Content-Type", "application/json")
    var t Token
    d.Msg = "Not a " + d.Method + " request or Request body is empty"
    method, params, err := RequestMethod(d.R)
    if method != d.Method || len(params) <= 2 || err != nil {
        return 400, Token{}, ""
    }
    if errT := json.Unmarshal([]byte(params), &t); errT != nil {
        d.Msg = fmt.Sprintf("Json format error: %v", errT)
        fmt.Println(fmt.Sprintf("Json format error: %v", params))
        return 400, Token{}, ""
    }
    return 200, t, params
}
func HelperMessage(hMsg *HelperMsg, gF func() []FunctionRouter) {
    fmt.Fprintf(hMsg.W, hMsg.Helper, hMsg.Msg, MapKey(gF(), hMsg.FunctionName), )
}

func (f FunctionRouter) Gen() (FunctionRouter) {
    v := reflect.ValueOf(f.Func)
    if v.Kind() == reflect.Func {
        if rf := runtime.FuncForPC(v.Pointer()); rf != nil {
            wHSlash := rf.Name()[strings.IndexByte(rf.Name(), '/')+1:]
            afterDots := wHSlash[strings.LastIndexByte(wHSlash, '.')+1:]
            f.GeneratedFuncName = strings.Replace(afterDots, "-fm", "", -1)
        }
    }
    return f
}

func MapKey(m []FunctionRouter, value string) (key string) {
    key = ""
    for _, funcR := range m {
        if funcR.GeneratedFuncName == value {
            key = funcR.RouteName
            return
        }
    }
    return
}
