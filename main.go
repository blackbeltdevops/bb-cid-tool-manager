package main

import (
    "net/http"
    "fmt"
    "bitbucket.org/blackbeltdevops/bb-cid-tool-manager/register"
)

func init() {
    register.Register()
}

func main() {
    fmt.Println("Server starting ... ")
    if err := http.ListenAndServe(":8080", nil); err != nil {
        panic(err)
    }
}
