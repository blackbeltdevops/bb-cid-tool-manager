package bbGithubApi

import (
    "bytes"
    "fmt"
    "io"
    "net/http"
    "net/http/httptest"
    "net/url"
    "os"
    "testing"

    "github.com/google/go-github/github"
    "github.com/stretchr/testify/assert"
    "io/ioutil"
)

const (
    // baseURLPath is a non-empty Client.BaseURL path to use during tests,
    // to ensure relative URLs are used for all endpoints. See issue #752.
    baseURLPath = "/api-v3"
)

func setup() (client *github.Client, mux *http.ServeMux, serverURL string, teardown func()) {
    // mux is the HTTP request multiplexer used with the test server.
    mux = http.NewServeMux()

    // We want to ensure that tests catch mistakes where the endpoint URL is
    // specified as absolute rather than relative. It only makes a difference
    // when there's a non-empty base URL path. So, use that. See issue #752.
    apiHandler := http.NewServeMux()
    apiHandler.Handle(baseURLPath+"/", http.StripPrefix(baseURLPath, mux))
    apiHandler.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
        fmt.Fprintln(os.Stderr, "FAIL: Client.BaseURL path prefix is not preserved in the request URL:")
        fmt.Fprintln(os.Stderr)
        fmt.Fprintln(os.Stderr, "\t"+req.URL.String())
        fmt.Fprintln(os.Stderr)
        fmt.Fprintln(os.Stderr, "\tDid you accidentally use an absolute endpoint URL rather than relative?")
        fmt.Fprintln(os.Stderr, "\tSee https://bbGithubGlobal-github.com/google/go-bbGithubGlobal-github/issues/752 for information.")
        http.Error(w, "Client.BaseURL path prefix is not preserved in the request URL.", http.StatusInternalServerError)
    })

    // server is a test HTTP server used to provide mock API responses.
    server := httptest.NewServer(apiHandler)

    // client is the GitHub client being tested and is
    // configured to use test server.
    client = github.NewClient(nil)
    cliUrl, _ := url.Parse(server.URL + baseURLPath + "/")
    client.BaseURL = cliUrl
    client.UploadURL = cliUrl

    return client, mux, server.URL, server.Close
}

type nopCloser struct {
    io.Reader
}

func acceptedStatusCode(code int) bool {
    return code >= 200 && code <= 300
}

func (nopCloser) Close() error { return nil }

func TestBBGithubAPi_InvitePush(t *testing.T) {
    cli, mux, _, teard := setup()
    defer teard()
    owner := "BlackBeltTechnology"
    repo := "ci-integrator"
    user := "buildfarm"
    var badReq bool
    //mux.HandleFunc("/repos/"+owner+"/"+repo+"/contents/"+user, func(w http.ResponseWriter, r *http.Request) {})
    mux.HandleFunc("/repos/"+owner+"/"+repo+"/collaborators/"+user, func(w http.ResponseWriter, r *http.Request) {
        if badReq {
            w.WriteHeader(http.StatusBadRequest)
        }
    })

    type fields struct {
        GhCli *github.Client
    }
    type args struct {
        w http.ResponseWriter
        r *http.Request
    }

    success := func(jsonAsStr string) (w *httptest.ResponseRecorder, r *http.Request) {
        w = httptest.NewRecorder()
        b := nopCloser{bytes.NewBufferString(jsonAsStr)}
        r = &http.Request{
            Method: "POST",
            URL:    &url.URL{Host: "/Invite/Push"},
            Body:   b,
        }
        return
    }
    jsonDefault := `{
    	"token" : "asdfasdfasdfasdfa", 
        "inviteData": { "owner":"%v", "repository": "%v", "user": "%v" }
    }`
    jsonWithToken := `{
    	"to" : "asdfasdfasdfasdfa" 
    }`
    wSucc, rSucc := success(fmt.Sprintf(jsonDefault, owner, repo, user))
    wSuccButApiFail, rSuccButApiFail := success(fmt.Sprintf(jsonDefault, owner, repo, user))
    wFail, rFail := success(fmt.Sprintf(jsonDefault, owner, repo, user))
    wFailWithToken, rFailWithToken := success(fmt.Sprintf(jsonWithToken))
    rFail.Method = "GET"
    wFailPut, rFailPut := success(fmt.Sprintf(jsonDefault, owner, repo, user))
    rFailPut.Method = "PUT"
    wNoBody, rNoBody := success("")
    wWrongBody, rWrongBody := success("{{{{{{{{{{")

    tests := []struct {
        name   string
        fields fields
        args   args
    }{
        {"success", fields{GhCli: cli}, args{wSucc, rSucc}},
        {"fail", fields{GhCli: cli}, args{wFail, rFail}},
        {"failPut", fields{GhCli: cli}, args{wFailPut, rFailPut}},
        {"failApi", fields{GhCli: cli}, args{wSuccButApiFail, rSuccButApiFail}},
        {"failNoBody", fields{GhCli: cli}, args{wNoBody, rNoBody}},
        {"failWrongBody", fields{GhCli: cli}, args{wWrongBody, rWrongBody}},
        {"FailWithToke", fields{GhCli: cli}, args{wFailWithToken, rFailWithToken}},
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            b := &BBGithubAPi{
                GhCli: tt.fields.GhCli,
            }
            if tt.name == "failApi" {
                badReq = true
            }
            b.InvitePush(tt.args.w, tt.args.r)
            fmt.Println(tt.args.w)
            if tt.name == "success" {
                fmt.Println(wSucc)
                assert.True(t, acceptedStatusCode(wSucc.Code))
            }
            if tt.name == "fail" {
                fmt.Println(wFail)
                assert.Equal(t, 400, wFail.Code)
            }
            if tt.name == "failPut" {
                fmt.Println(wFailPut)
                assert.Equal(t, 400, wFailPut.Code)
            }
            if tt.name == "failApi" {
                fmt.Println(wSuccButApiFail)
                assert.Equal(t, 400, wSuccButApiFail.Code)
            }
            if tt.name == "failNoBody" {
                fmt.Println(wNoBody)
                assert.Equal(t, 400, wNoBody.Code)
            }
            if tt.name == "failWrongBody" {
                fmt.Println(wWrongBody)
                assert.Equal(t, 400, wWrongBody.Code)
            }
        })
    }
}

func TestBBGithubAPi_InvitePull(t *testing.T) {
    cli, mux, _, teard := setup()
    defer teard()
    owner := "BlackBeltTechnology"
    repo := "ci-integrator"
    user := "buildfarm"
    var badReq bool
    mux.HandleFunc("/repos/"+owner+"/"+repo+"/collaborators/"+user, func(w http.ResponseWriter, r *http.Request) {
        if badReq {
            w.WriteHeader(http.StatusBadRequest)
        }
    })

    type fields struct {
        GhCli *github.Client
    }
    type args struct {
        w http.ResponseWriter
        r *http.Request
    }

    success := func(jsonAsStr string) (w *httptest.ResponseRecorder, r *http.Request) {
        w = httptest.NewRecorder()
        b := nopCloser{bytes.NewBufferString(jsonAsStr)}
        r = &http.Request{
            Method: "POST",
            URL:    &url.URL{Host: "/Invite/Pull"},
            Body:   b,
        }
        return
    }
    jsonDefault := `{
    	"token" : "asdfasdfasdfasdfa", 
        "inviteData": { "owner":"%v", "repository": "%v", "user": "%v" }
    }`
    wSucc, rSucc := success(fmt.Sprintf(jsonDefault, owner, repo, user))
    wSuccButApiFail, rSuccButApiFail := success(fmt.Sprintf(jsonDefault, owner, repo, user))
    wFail, rFail := success(fmt.Sprintf(jsonDefault, owner, repo, user))
    rFail.Method = "GET"
    wFailPut, rFailPut := success(fmt.Sprintf(jsonDefault, owner, repo, user))
    rFailPut.Method = "PUT"
    wNoBody, rNoBody := success("")
    wWrongBody, rWrongBody := success("{{{{{{{{{{")

    tests := []struct {
        name   string
        fields fields
        args   args
    }{
        {"success", fields{GhCli: cli}, args{wSucc, rSucc}},
        {"fail", fields{GhCli: cli}, args{wFail, rFail}},
        {"failPut", fields{GhCli: cli}, args{wFailPut, rFailPut}},
        {"failApi", fields{GhCli: cli}, args{wSuccButApiFail, rSuccButApiFail}},
        {"failNoBody", fields{GhCli: cli}, args{wNoBody, rNoBody}},
        {"failWrongBody", fields{GhCli: cli}, args{wWrongBody, rWrongBody}},
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            b := &BBGithubAPi{
                GhCli: tt.fields.GhCli,
            }
            if tt.name == "failApi" {
                badReq = true
            }
            b.InvitePull(tt.args.w, tt.args.r)
            if tt.name == "success" {
                fmt.Println(wSucc)
                assert.True(t, acceptedStatusCode(wSucc.Code))
            }
            if tt.name == "fail" {
                fmt.Println(wFail)
                assert.Equal(t, 400, wFail.Code)
            }
            if tt.name == "failPut" {
                fmt.Println(wFailPut)
                assert.Equal(t, 400, wFailPut.Code)
            }
            if tt.name == "failApi" {
                fmt.Println(wSuccButApiFail)
                assert.Equal(t, 400, wSuccButApiFail.Code)
            }
            if tt.name == "failNoBody" {
                fmt.Println(wNoBody)
                assert.Equal(t, 400, wNoBody.Code)
            }
            if tt.name == "failWrongBody" {
                fmt.Println(wWrongBody)
                assert.Equal(t, 400, wWrongBody.Code)
            }
        })
    }
}

func TestBBGithubAPi_InviteAdmin(t *testing.T) {
    cli, mux, _, teard := setup()
    defer teard()
    owner := "BlackBeltTechnology"
    repo := "ci-integrator"
    user := "buildfarm"
    var badReq bool
    mux.HandleFunc("/repos/"+owner+"/"+repo+"/collaborators/"+user, func(w http.ResponseWriter, r *http.Request) {
        if badReq {
            w.WriteHeader(http.StatusBadRequest)
        }
    })

    type fields struct {
        GhCli *github.Client
    }
    type args struct {
        w http.ResponseWriter
        r *http.Request
    }

    success := func(jsonAsStr string) (w *httptest.ResponseRecorder, r *http.Request) {
        w = httptest.NewRecorder()
        b := nopCloser{bytes.NewBufferString(jsonAsStr)}
        r = &http.Request{
            Method: "POST",
            URL:    &url.URL{Host: "/Invite/Admin"},
            Body:   b,
        }
        return
    }
    //assert.Equal(t, 200, wSucc.Code, )

    jsonDefault := `{
    	"token" : "asdfasdfasdfasdfa", 
        "inviteData": { "owner":"%v", "repository": "%v", "user": "%v" }
    }`
    wSucc, rSucc := success(fmt.Sprintf(jsonDefault, owner, repo, user))
    wSuccButApiFail, rSuccButApiFail := success(fmt.Sprintf(jsonDefault, owner, repo, user))
    wFail, rFail := success(fmt.Sprintf(jsonDefault, owner, repo, user))
    rFail.Method = "GET"
    wFailPut, rFailPut := success(fmt.Sprintf(jsonDefault, owner, repo, user))
    rFailPut.Method = "PUT"
    wNoBody, rNoBody := success("")
    wWrongBody, rWrongBody := success("{{{{{{{{{{")

    tests := []struct {
        name   string
        fields fields
        args   args
    }{
        {"success", fields{GhCli: cli}, args{wSucc, rSucc}},
        {"fail", fields{GhCli: cli}, args{wFail, rFail}},
        {"failPut", fields{GhCli: cli}, args{wFailPut, rFailPut}},
        {"failApi", fields{GhCli: cli}, args{wSuccButApiFail, rSuccButApiFail}},
        {"failNoBody", fields{GhCli: cli}, args{wNoBody, rNoBody}},
        {"failWrongBody", fields{GhCli: cli}, args{wWrongBody, rWrongBody}},
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            b := &BBGithubAPi{
                GhCli: tt.fields.GhCli,
            }
            if tt.name == "failApi" {
                badReq = true
            }
            b.InviteAdmin(tt.args.w, tt.args.r)
            if tt.name == "success" {
                fmt.Println(wSucc)
                assert.True(t, acceptedStatusCode(wSucc.Code))
            }
            if tt.name == "fail" {
                fmt.Println(wFail)
                assert.Equal(t, 400, wFail.Code)
            }
            if tt.name == "failPut" {
                fmt.Println(wFailPut)
                assert.Equal(t, 400, wFailPut.Code)
            }
            if tt.name == "failApi" {
                fmt.Println(wSuccButApiFail)
                assert.Equal(t, 400, wSuccButApiFail.Code)
            }
            if tt.name == "failNoBody" {
                fmt.Println(wNoBody)
                assert.Equal(t, 400, wNoBody.Code)
            }
            if tt.name == "failWrongBody" {
                fmt.Println(wWrongBody)
                assert.Equal(t, 400, wWrongBody.Code)
            }
        })
    }
}

func TestBBGithubAPi_AcceptAllInvitations(t *testing.T) {
    cli, mux, _, teard := setup()
    defer teard()

    var badInvGet bool
    var badListInv bool
    mux.HandleFunc("/user/repository_invitations", func(w http.ResponseWriter, r *http.Request) {
        if badListInv {
            w.WriteHeader(http.StatusBadRequest)
        } else {
            fmt.Fprintf(w, `[{"id":1}, {"id":2}]`)
        }
    })
    mux.HandleFunc("/user/repository_invitations/1", func(w http.ResponseWriter, r *http.Request) {
        if badInvGet {
            w.WriteHeader(http.StatusBadRequest)
        } else {
            w.WriteHeader(http.StatusNoContent)
        }
    })
    mux.HandleFunc("/user/repository_invitations/2", func(w http.ResponseWriter, r *http.Request) {
        if badInvGet {
            w.WriteHeader(http.StatusBadRequest)
        } else {
            w.WriteHeader(http.StatusNoContent)
        }
    })

    type fields struct {
        GhCli *github.Client
    }
    type args struct {
        w http.ResponseWriter
        r *http.Request
    }

    success := func(jsonAsStr string) (w *httptest.ResponseRecorder, r *http.Request) {
        w = httptest.NewRecorder()
        b := nopCloser{bytes.NewBufferString(jsonAsStr)}
        r = &http.Request{
            Method: "POST",
            URL:    &url.URL{Host: "/AcceptAllInvitations"},
            Body:   b,
        }
        return
    }
    jsonDefault := `{
    	"token" : "asdfasdfasdfasdfa"
    }`
    wSucc, rSucc := success(jsonDefault)
    wSuccButApiFail, rSuccButApiFail := success(jsonDefault)
    wSuccButListInvFail, rSuccButListInvFail := success(jsonDefault)
    wFail, rFail := success(jsonDefault)
    rFail.Method = "GET"
    wFailPut, rFailPut := success(jsonDefault)
    rFailPut.Method = "PUT"
    wNoBody, rNoBody := success("")
    wWrongBody, rWrongBody := success("{{{{{{{{{{")

    tests := []struct {
        name   string
        fields fields
        args   args
    }{
        {"success", fields{GhCli: cli}, args{wSucc, rSucc}},
        {"fail", fields{GhCli: cli}, args{wFail, rFail}},
        {"failPut", fields{GhCli: cli}, args{wFailPut, rFailPut}},
        {"failApi", fields{GhCli: cli}, args{wSuccButApiFail, rSuccButApiFail}},
        {"failListInv", fields{GhCli: cli}, args{wSuccButListInvFail, rSuccButListInvFail}},
        {"failNoBody", fields{GhCli: cli}, args{wNoBody, rNoBody}},
        {"failWrongBody", fields{GhCli: cli}, args{wWrongBody, rWrongBody}},
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            b := &BBGithubAPi{
                GhCli: tt.fields.GhCli,
            }
            if tt.name == "failApi" {
                badInvGet = true
            }
            if tt.name == "failListInv" {
                badListInv = true
            }
            b.AcceptAllInvitations(tt.args.w, tt.args.r)
            if tt.name == "success" {
                fmt.Println(wSucc)
                assert.True(t, acceptedStatusCode(wSucc.Code))
            }
            if tt.name == "fail" {
                fmt.Println(wFail)
                assert.Equal(t, 400, wFail.Code)
            }
            if tt.name == "failPut" {
                fmt.Println(wFailPut)
                assert.Equal(t, 400, wFailPut.Code)
            }
            if tt.name == "failApi" {
                badInvGet = false
                fmt.Println(wSuccButApiFail)
                assert.Equal(t, 400, wSuccButApiFail.Code)
            }
            if tt.name == "failListInv" {
                badListInv = false
                fmt.Println(wSuccButListInvFail)
                assert.Equal(t, 400, wSuccButListInvFail.Code)
            }
            if tt.name == "failNoBody" {
                fmt.Println(wNoBody)
                assert.Equal(t, 400, wNoBody.Code)
            }
            if tt.name == "failWrongBody" {
                fmt.Println(wWrongBody)
                assert.Equal(t, 400, wWrongBody.Code)
            }
        })
    }
}

func TestBBGithubAPi_AddDefaultPipeline(t *testing.T) {
    cli, mux, _, teard := setup()
    defer teard()

    var badReq bool
    owner := "BlackBeltTechnology"
    repo := "ci-integrator"
    path := ".buildkite/pipeline.yml"

    mux.HandleFunc("/repos/"+owner+"/"+repo+"/contents/"+path, func(w http.ResponseWriter, r *http.Request) {
        if badReq {
            w.WriteHeader(http.StatusBadRequest)
        } else {
            w.WriteHeader(http.StatusNoContent)
        }
    })
    type fields struct {
        GhCli *github.Client
    }
    type args struct {
        w http.ResponseWriter
        r *http.Request
    }

    success := func(jsonAsStr string) (w *httptest.ResponseRecorder, r *http.Request) {
        w = httptest.NewRecorder()
        b := nopCloser{bytes.NewBufferString(jsonAsStr)}
        r = &http.Request{
            Method: "POST",
            URL:    &url.URL{Host: "/AddDefaultPipeline"},
            Body:   b,
        }
        return
    }
    var repoContentOpts = `{"message":"Add default pipeline yaml file","content":"IyAuYnVpbGRraXRlL3BpcGVsaW5lLnltbApzdGVwczoKICAtIG5hbWU6ICI6aGFtbWVyOiBidWlsZCAiCiAgICBjb21tYW5kOiBlY2hvICJIZWxsbyB3b3JsZCIKIyAgIGFnZW50czoKIyAgICAgIHRhcmdldDogYXNzZW1ibGVyCg==","branch":"master","committer":{"name":"BuildFarm","email":"bbGithubGlobal-buildfarm@blackbelt.hu"}}`

    jsonDefault := `{
    	"token" : "asdfasdfasdfasdfa", 
        "fileCreatingData": { "owner":"%v", "repository": "%v", "path": "%v" },
        "repoContentOpts" : %v
    }`
    wSucc, rSucc := success(fmt.Sprintf(jsonDefault, owner, repo, path, repoContentOpts))
    wSuccButApiFail, rSuccButApiFail := success(fmt.Sprintf(jsonDefault, owner, repo, path, repoContentOpts))
    wFail, rFail := success(fmt.Sprintf(jsonDefault, owner, repo, path, repoContentOpts))
    rFail.Method = "GET"
    wFailPut, rFailPut := success(fmt.Sprintf(jsonDefault, owner, repo, path, repoContentOpts))
    rFailPut.Method = "PUT"
    wNoBody, rNoBody := success("")
    wWrongBody, rWrongBody := success("{{{{{{{{{{")

    tests := []struct {
        name   string
        fields fields
        args   args
    }{
        {"success", fields{GhCli: cli}, args{wSucc, rSucc}},
        {"fail", fields{GhCli: cli}, args{wFail, rFail}},
        {"failPut", fields{GhCli: cli}, args{wFailPut, rFailPut}},
        {"failApi", fields{GhCli: cli}, args{wSuccButApiFail, rSuccButApiFail}},
        {"failNoBody", fields{GhCli: cli}, args{wNoBody, rNoBody}},
        {"failWrongBody", fields{GhCli: cli}, args{wWrongBody, rWrongBody}},
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            b := &BBGithubAPi{
                GhCli: tt.fields.GhCli,
            }
            if tt.name == "failApi" {
                badReq = true
            }
            b.AddDefaultPipeline(tt.args.w, tt.args.r)
            if tt.name == "success" {
                fmt.Println(wSucc)
                assert.True(t, acceptedStatusCode(wSucc.Code))
            }
            if tt.name == "fail" {
                fmt.Println(wFail)
                assert.Equal(t, 400, wFail.Code)
            }
            if tt.name == "failPut" {
                fmt.Println(wFailPut)
                assert.Equal(t, 400, wFailPut.Code)
            }
            if tt.name == "failApi" {
                badReq = false
                fmt.Println(wSuccButApiFail)
                assert.Equal(t, 400, wSuccButApiFail.Code)
            }
            if tt.name == "failNoBody" {
                fmt.Println(wNoBody)
                assert.Equal(t, 400, wNoBody.Code)
            }
            if tt.name == "failWrongBody" {
                fmt.Println(wWrongBody)
                assert.Equal(t, 400, wWrongBody.Code)
            }
        })
    }
}

func TestBBGithubAPi_CreateHook(t *testing.T) {
    cli, mux, serverUrl, teard := setup()
    defer teard()

    //var badReq bool
    owner := "BlackBeltTechnology"
    repo := "ci-integrator"
    testOrg := "BlackBeltTechnology"

    jsonDefault := `{
        "token"         : "asdfasdfasdfasdfa", 
            "buildkiteData" : {
                "token"          : "__buildkite__token___",
                "organization"   : "%v"
            },
            "hookCreateData" : {
                "owner"          : "%v",
                "repository"     : "%v"
            },
            "hook" : {
                "name": "web",
                "active": "true",
                "events": [
                  "push",
                  "pull_request"
                ]
            }
    }`

    mux.HandleFunc("/repos/"+owner+"/"+repo+"/hooks", func(w http.ResponseWriter, r *http.Request) {
        fmt.Fprint(w, `{"id":1}`)
    })

    mux.HandleFunc("/ListPipeline", func(w http.ResponseWriter, r *http.Request) {
        //fmt.Fprint(w, `{"sa":"sa"}`)
        fmt.Fprint(w, `[ { "repository": "fafa", "webhook_url": "1234" } ]`)

        fmt.Println("tetetetetetetetettsetstst")
        defer r.Body.Close()
        body, _ := ioutil.ReadAll(r.Body)
        fmt.Println(string(body))
    })

    success := func(jsonAsStr string) (w *httptest.ResponseRecorder, r *http.Request) {
        w = httptest.NewRecorder()
        b := nopCloser{bytes.NewBufferString(jsonAsStr)}
        r = &http.Request{
            Method: "POST",
            URL:    &url.URL{Host: "/CreateHook"},
            Body:   b,
        }
        return
    }

    wSucc, rSucc := success(fmt.Sprintf(jsonDefault, testOrg, owner, repo))

    fmt.Println(serverUrl)

    type fields struct {
        GhCli *github.Client
        bkRequest struct {
            cli *http.Client
            url string
        }
    }
    fi := fields{GhCli: cli}
    fi.bkRequest.cli = http.DefaultClient
    fi.bkRequest.url = serverUrl + baseURLPath
    type args struct {
        w http.ResponseWriter
        r *http.Request
    }
    tests := []struct {
        name   string
        fields fields
        args   args
    }{
        {"success", fi, args{wSucc, rSucc}},
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            b := &BBGithubAPi{
                GhCli:     tt.fields.GhCli,
                bkRequest: tt.fields.bkRequest,
            }
            b.CreateHook(tt.args.w, tt.args.r)
        })
    }
}
