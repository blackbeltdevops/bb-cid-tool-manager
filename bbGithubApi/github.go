package bbGithubApi

import (
    "net/http"
    "github.com/google/go-github/github"
    "bitbucket.org/blackbeltdevops/bb-github-api/bb-github"
    "bitbucket.org/blackbeltdevops/bb-cid-tool-manager/lib"
    "log"
    "bitbucket.org/blackbeltdevops/bb-github-api/api"
    "encoding/json"
    "fmt"
    "golang.org/x/oauth2"
    "context"
    "github.com/buildkite/go-buildkite/buildkite"
)

var (
    bbGithubGlobal = BBGithubAPi{}
)

type BBGithubAPi struct {
    GhCli *github.Client
    bkRequest struct {
        cli *http.Client
        url string
    }
}

type GithubApiFunc struct {
    Api func(t lib.Token, params string) (*github.Response, error)
}

type inviteData struct {
    lib.DataFlowManager
    GithubApiFunc
    Permission func(BBGh bb_github.BBGithub, params string) (*github.Response, error)
}
type acceptAllInvitationData struct {
    lib.DataFlowManager
    GithubApiFunc
    AcceptAll func(BBGh bb_github.BBGithub, params string) (*github.Response, error)
}
type fileCreateData struct {
    lib.DataFlowManager
    GithubApiFunc
    CreateFile func(BBGh bb_github.BBGithub, params string) (*github.Response, error)
}
type hookCreateData struct {
    lib.DataFlowManager
    GithubApiFunc
    CreateHook func(BBGh bb_github.BBGithub, params string) (*github.Response, error)
}

func GetFuncRouter() []lib.FunctionRouter {
    return [] lib.FunctionRouter{
        lib.FunctionRouter{RouteName: "/Invite/Push", Func: bbGithubGlobal.InvitePush}.Gen(),
        lib.FunctionRouter{RouteName: "/Invite/Pull", Func: bbGithubGlobal.InvitePull}.Gen(),
        lib.FunctionRouter{RouteName: "/Invite/Admin", Func: bbGithubGlobal.InviteAdmin}.Gen(),
        lib.FunctionRouter{RouteName: "/AcceptAllInvitations", Func: bbGithubGlobal.AcceptAllInvitations}.Gen(),
        lib.FunctionRouter{RouteName: "/AddDefaultPipeline", Func: bbGithubGlobal.AddDefaultPipeline}.Gen(),
        lib.FunctionRouter{RouteName: "/CreateWebHook", Func: bbGithubGlobal.CreateHook}.Gen(),
    }
}

func createApi(b *BBGithubAPi) (tmp *BBGithubAPi) {
    if (*b == BBGithubAPi{}) {
        tmp = new(BBGithubAPi)
        log.Println("Session id:", lib.MD5(tmp))
        return
    }
    return b
}

func (b *BBGithubAPi) deleteApi() {
    log.Println("Remove session:", lib.MD5(b))
    *b = BBGithubAPi{}
}

func (b *BBGithubAPi) MakeContextAndClient(token string) {
    b.makeContextAndClient(token)
}

func (b *BBGithubAPi) makeContextAndClient(token string) {
    if b.GhCli == nil {
        b.bkRequest.cli = oauth2.NewClient(context.Background(), nil)
        b.bkRequest.url = "http://localhost:8080/"

        cli := bb_github.MakeHttpClient(token)
        b.GhCli = bb_github.GetGithubClient(cli)
    }
}

func (b *BBGithubAPi) reqHan(d *lib.DataFlowManager, dApi GithubApiFunc) (statCode int) {
    statCode, t, params := lib.RequestHandler(d)
    if statCode >= 200 && statCode < 300 {
        resp, err := dApi.Api(t, params)
        if resp.StatusCode < 200 || resp.StatusCode >= 300 {
            d.Msg = d.FunctionName + " failed, reason: " + err.Error()
        }
        statCode = resp.StatusCode
    }
    b.deleteApi()
    d.W.WriteHeader(statCode)
    return
}
func (b *BBGithubAPi) invite(d *inviteData) {
    d.Api = func(t lib.Token, params string) (*github.Response, error) {
        b.makeContextAndClient(t.Token)
        BBGh := bb_github.NewClientWithGithubClient(b.GhCli)
        return d.Permission(BBGh, params)
    }
    statCode := b.reqHan(&d.DataFlowManager, d.GithubApiFunc)
    if statCode >= 200 && statCode < 300 {
        return
    }
    hMsg := lib.HelperMsg{W: d.W, Helper: ` {
        "msg" : "%v",
        "url" : "%v",
        "JsonToBeSent" : {
            "token"      :       "< Token for github api >",
            "inviteData" : {
                "owner"     :    "< Organization name >",
                "repository":    "< PipelineName name >",
                "user"      :    "< Invited user name >"
            }
        }
    } `, Msg: d.Msg, FunctionName: d.FunctionName}

    lib.HelperMessage(&hMsg, GetFuncRouter)
}
func (b *BBGithubAPi) accept(d *acceptAllInvitationData) {
    d.Api = func(t lib.Token, params string) (*github.Response, error) {
        b.makeContextAndClient(t.Token)
        BBGh := bb_github.NewClientWithGithubClient(b.GhCli)
        return d.AcceptAll(BBGh, params)
    }
    statCode := b.reqHan(&d.DataFlowManager, d.GithubApiFunc)
    if statCode >= 200 && statCode < 300 {
        return
    }
    hMsg := lib.HelperMsg{W: d.W, Helper: ` {
        "msg": "%v",
        "url" : "%v",
        "JsonToBeSent" : {
            "token" :       "< Token for github api >"
        }
    } `, Msg: d.Msg, FunctionName: d.FunctionName}

    lib.HelperMessage(&hMsg, GetFuncRouter)
}
func (b *BBGithubAPi) create(d *fileCreateData) {
    d.Api = func(t lib.Token, params string) (*github.Response, error) {
        b.makeContextAndClient(t.Token)
        BBGh := bb_github.NewClientWithGithubClient(b.GhCli)
        return d.CreateFile(BBGh, params)
    }
    statCode := b.reqHan(&d.DataFlowManager, d.GithubApiFunc)
    if statCode >= 200 && statCode < 300 {
        return
    }
    hMsg := lib.HelperMsg{W: d.W, Helper: ` {
        "msg": "%v",
        "url" : "%v",
        "JsonToBeSent" : {
            "token" :       "< Token for github api >",
            "fileCreatingData" : {
                "owner":        "< Organization name >",
                "repository":   "< PipelineName name >",
                "path":         "< File path, eg.: /path/to/my/file.yml >"
            }
        }
    } `, Msg: d.Msg, FunctionName: d.FunctionName}

    lib.HelperMessage(&hMsg, GetFuncRouter)
}
func (b *BBGithubAPi) hook(d *hookCreateData) {
    d.Api = func(t lib.Token, params string) (*github.Response, error) {
        b.makeContextAndClient(t.Token)
        BBGh := bb_github.NewClientWithGithubClient(b.GhCli)
        return d.CreateHook(BBGh, params)
    }
    statCode := b.reqHan(&d.DataFlowManager, d.GithubApiFunc)
    if statCode >= 200 && statCode < 300 {
        return
    }
    hMsg := lib.HelperMsg{W: d.W, Helper: ` {
        "msg": "%v",
        "url" : "%v",
        "JsonToBeSent" : {
            "token"         :  "< Token for github api >",
            "hookCreateData" : {
                "owner":        "< Organization name >",
                "repository":   "< PipelineName name >"
            },
            "hook" : {
                "name": "web",
                "active": "< true / false >",
                "events": [
                  "push",
                  "pull_request"
                ]
            }
        }
    } `, Msg: d.Msg, FunctionName: d.FunctionName}

    lib.HelperMessage(&hMsg, GetFuncRouter)
}

func (b *BBGithubAPi) InvitePush(w http.ResponseWriter, r *http.Request) {
    b = createApi(b)
    d := inviteData{lib.DataFlowManager{"InvitePush", lib.POST, "", w, r,}.FillMsg(), GithubApiFunc{}, nil}
    d.Permission = func(BBGh bb_github.BBGithub, params string) (*github.Response, error) {
        return BBGh.InviteWithJson(params).Push()
    }
    b.invite(&d)
}
func (b *BBGithubAPi) InvitePull(w http.ResponseWriter, r *http.Request) {
    b = createApi(b)
    d := inviteData{lib.DataFlowManager{"InvitePull", lib.POST, "", w, r,}.FillMsg(), GithubApiFunc{}, nil}
    d.Permission = func(BBGh bb_github.BBGithub, params string) (*github.Response, error) {
        return BBGh.InviteWithJson(params).Pull()
    }
    b.invite(&d)
}
func (b *BBGithubAPi) InviteAdmin(w http.ResponseWriter, r *http.Request) {
    b = createApi(b)
    d := inviteData{lib.DataFlowManager{"InviteAdmin", lib.POST, "", w, r,}.FillMsg(), GithubApiFunc{}, nil}
    d.Permission = func(BBGh bb_github.BBGithub, params string) (*github.Response, error) {
        return BBGh.InviteWithJson(params).Admin()
    }
    b.invite(&d)
}

func (b *BBGithubAPi) CreateHook(w http.ResponseWriter, r *http.Request) {
    b = createApi(b)
    d := hookCreateData{lib.DataFlowManager{"CreateHook", lib.POST, "", w, r,}.FillMsg(), GithubApiFunc{}, nil}
    d.CreateHook = func(bApi bb_github.BBGithub, params string) (resp *github.Response, err error) {
        hook := api.CreateHookWithJson(params)
        f := func() (*bool) { var tr = true; return &tr }
        hook.Hook.Name = buildkite.String("web")
        hook.Hook.Active = f()

        temp := func() {
            type buildkiteData struct {
                Token        string `json:"token"`
                Organization string `json:"organization"`
            }
            payload := lib.BKD{}.BuildkiteData
            pp := lib.BKD{}
            json.Unmarshal([]byte(params), &pp)
            payload.Organization = pp.BuildkiteData.Organization
            payload.Token = pp.BuildkiteData.Token
            p, _ := json.Marshal(payload)
            //p:=""
            fmt.Println("ffffffffffffffff", pp.BuildkiteData)
            //b.GhCli

            bkResp, bkErr, bkBody := lib.Request(b.bkRequest.cli, "POST",
                fmt.Sprintf("%v/ListPipeline", b.bkRequest.url),
                string(p),
                "application/json",
            )
            fmt.Println(bkResp)
            fmt.Println(bkErr)
            fmt.Println(bkBody)
        }
        fmt.Println(temp)

        // TODO: get webhooks url from buildkite ...
        // maybe need from github too -____-
        hook.Hook.Config = map[string]interface{}{"url": "url", "content_type": "json"}
        _, resp, err = bApi.CreateWebHook(hook)
        return
    }
    b.hook(&d)
}

func (b *BBGithubAPi) AcceptAllInvitations(w http.ResponseWriter, r *http.Request) {
    b = createApi(b)
    d := acceptAllInvitationData{lib.DataFlowManager{"AcceptAllInvitations", lib.POST, "", w, r,}.FillMsg(), GithubApiFunc{}, nil}
    d.AcceptAll = func(BBGh bb_github.BBGithub, params string) (*github.Response, error) {
        repoInvs, invResp, invError := BBGh.Invitations()
        utErrors := BBGh.AcceptAllInvitations(repoInvs, invResp, invError)
        for _, ue := range utErrors {
            if ue.Err != nil {
                return ue.Resp, ue.Err
            }
        }
        return utErrors[0].Resp, utErrors[0].Err
    }
    b.accept(&d)
}

func (b *BBGithubAPi) AddDefaultPipeline(w http.ResponseWriter, r *http.Request) {
    b = createApi(b)
    d := fileCreateData{lib.DataFlowManager{"AddDefaultPipeline", lib.POST, "", w, r,}.FillMsg(), GithubApiFunc{}, nil}
    d.CreateFile = func(BBGh bb_github.BBGithub, params string) (respons *github.Response, err error) {
        _, respons, err = BBGh.AddPipelineWithJson(params)
        return
    }
    b.create(&d)
}
